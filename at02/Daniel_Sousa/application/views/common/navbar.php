
<nav class="navbar navbar-expand-lg navbar-dark primary-color">

  <a class="navbar-brand" href="#">IF"SPeech"</a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="basicExampleNav">

    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('Api/')?>">Home
          <span class="sr-only">(current)</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('Api/sobreAApi')?>">Sobre a API</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('Api/uploadVideo')?>">Upload de Aúdio</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('Api/relatorio') ?>">Relátorio</a>
      </li>

    </ul>
  </div>


</nav>

