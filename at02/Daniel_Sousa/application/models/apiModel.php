<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/TranscreverApi.php';

class ApiModel extends CI_Model{
    public function visualizar($audio, $language){
        $speech = new TranscreverApi();
        $transcrever = $speech->speech($audio, $language);
        return $transcrever;
    }
    public function upload(){
        $language = $this->input->post('language');
		$config['upload_path'] = './assets/audio';
        $config['allowed_types'] = '*';
        $this->load->library('upload', $config);

			if(!$this->upload->do_upload('audio')){
			$error = array('error' => $this->upload->display_errors());
		}
		else{
            
            $file_data = $this->upload->data();
            $data = array("audio" => $file_data['file_name'], "language" => $language);
            $this->db->insert('audios', $data);
            echo '<script>alert("Upload realizado com sucesso!")</script>';
            redirect('Api/visualizar/'.$file_data['file_name'].'/'.$language);
		}

    }
    public function relatorio(){
        $sql = "SELECT * FROM audios";
        $res = $this->db->query($sql);
        $data = $res->result();
        return $data;
    }
}