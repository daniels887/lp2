<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-6">
            <form class="text-center p-5" method="POST" enctype="multipart/form-data">
                <p class="h4 mb-4">Faça upload do seu áudio no formato (FLAC) <p>
                    <select class="browser-default custom-select mb-4" name="language" required>
                        <option value="" disabled selected>Escolha o idioma do áudio</option>
                        <option value="pt-BR">pt-BR</option>
                        <option value="en-US">en-US</option>
                    </select>
                    <input type="file" id="audio" name="audio" class="form-control mb-4" required>
                <button class="btn btn-dark btn-block" type="submit">Cadastrar</button>
            </form>
        </div>
    </div>
</div>