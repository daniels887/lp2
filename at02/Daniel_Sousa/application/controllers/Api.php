<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function index()
	{
		$this->load->view('common/header.php');
		$this->load->view('common/navbar.php');
		$this->load->view('Home/index.php');
	}
	public function visualizar($audio, $language){
		$this->load->view('common/header.php');
		$this->load->view('common/navbar.php');
		$this->load->model('ApiModel');
		$visualizar['transcrito'] = $this->ApiModel->visualizar($audio, $language);
		$this->load->view('Visualizar/visualizar.php', $visualizar);
	}
	public function sobreAApi()
	{
		$this->load->view('common/header.php');
		$this->load->view('common/navbar.php');
		$this->load->view('Sobre/sobreAApi.php');
	}
	public function uploadVideo(){
		$this->load->view('common/header.php');
		$this->load->view('common/navbar.php');
		$this->load->model('ApiModel');
		$this->ApiModel->upload();
		$this->load->view('Upload/uploadAudio.php');
	}
	public function relatorio(){
		$this->load->view('common/header.php');
		$this->load->view('common/navbar.php');
		$this->load->model('ApiModel');
		$data['resultados'] = $this->ApiModel->relatorio();
		$this->load->view('Relatorio/tableRelatorio.php', $data);		
	}
}
