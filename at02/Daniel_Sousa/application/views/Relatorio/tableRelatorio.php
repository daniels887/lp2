<div class="container mt-5">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Aúdio</th>
                <th scope="col">Idioma</th>
                <th scope="col">Última modificação</th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($resultados as $resultado): ?>
            <a href="#">
                <tr>
                    <th scope="row"><?= $resultado->id ?></th>
                    <td><?= $resultado->audio ?></td>
                    <td><?= $resultado->language?></td>
                    <td><?= $resultado->ultima_modificacao ?></td>
                    <td><a href="<?= base_url('Api/visualizar/'.$resultado->audio.'/'.$resultado->language.'') ?>">
                    <i class="fas fa-eye"></i></a>
                    </td>
                </tr>
                <?php endforeach; ?>
        </tbody>
    </table>
</div>