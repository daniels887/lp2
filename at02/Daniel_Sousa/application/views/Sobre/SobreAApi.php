<div class="jumbotron mt-4"
    style="background-image: url('<?= base_url('assets/img/background.png')?>');background-repeat: no-repeat; background-size: cover;">
    <div class="text-center text-white py-5 px-4 my-5">
        <div class="mt-3 pt-5">
            <h2 class="card-title h1-responsive pt-3 mb-5 font-bold text-white"><strong>Google Speech API</strong></h2>
            <ul class="nav nav-tabs d-flex justify-content-center" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#carac" role="tab"
                        aria-controls="home" aria-selected="true">Características e finalidades</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#criar" role="tab"
                        aria-controls="profile" aria-selected="false">O que é possível criar?</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#restricoes" role="tab"
                        aria-controls="contact" aria-selected="false">Restrições para o uso da API</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#passos" role="tab"
                        aria-controls="contact" aria-selected="false">Passos necessários para a implementação da API</a>
                </li>
            </ul>
            <div class="tab-content mt-5" id="myTabContent">
                <div class="tab-pane fade show active" id="carac" role="tabpanel" aria-labelledby="home-tab">Google
                    Speech API é uma tecnologia criada para
                    transcrever aúdios para texto, sendo o aúdio inicialmente um arquivo ou capturado pelo microfone,
                    sendo esse último não
                    utilizado nesse projeto.</div>
                <div class="tab-pane fade" id="criar" role="tabpanel" aria-labelledby="profile-tab">É possível criar
                    projetos que necessitam
                    de transcrição de aúdio em texto, para inclusão social de deficientes auditivos entre outros.</div>
                <div class="tab-pane fade" id="restricoes" role="tabpanel" aria-labelledby="contact-tab">Para utilizar
                    essa API, é necessário acesso
                    ao google cloud, pois é utilizado chave de acesso (key) para validação da API em projetos. A Api
                    suporta
                    melhor 2 extensões de aúdio: FLAC e Raw, ela é dividida em 2, com streaming e sem, sendo o primeiro
                    utilizando microfone e o segundo arquivos
                    de aúdio.</div>
                <div class="tab-pane fade" id="passos" role="tabpanel" aria-labelledby="contact-tab">Para utilizar essa
                    API, é necessário acesso
                    ao google cloud, pois é utilizado chave de acesso (key) para validação da API em projetos. A Api
                    suporta
                    melhor 2 extensões de aúdio: FLAC e Raw, ela é dividida em 2, com streaming e sem, sendo o primeiro
                    utilizando microfone e o segundo arquivos
                    de aúdio. A Api utilizada nesse projeto transcreve arquivos com até 1 minuto de duração. Esse projeto foi feito
                    utilizando o composer, e com upload de aúdio</div>
            </div>
        </div>
    </div>
</div>