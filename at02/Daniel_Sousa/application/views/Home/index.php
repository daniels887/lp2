<div class="container mt-5">
    <h1 class="mt-5 mb-5 gradient text-center">Cloud Speech API</h1>
    <div class="jumbotron text-center hoverable">
        <div class="row">
            <div class="col-md-3">

                <img src="<?= base_url('assets/img/daniel.jpg')?>" class="img-fluid">
                <a>
                    <div class="mask rgba-white-slight"></div>
                </a>

            </div>
            <div class="col-md-6  mt-3">
                <h4 class="h4 mb-4">Daniel de Sousa Pereira</h4>
                <p class="font-weight-normal">Prontuário: GU3002438</p>
                <p class="font-weight-normal">3º Semestre do curso de ADS</p>
            </div>
            <div class="col-md-3">
                <img src="<?= base_url('assets/img/speech.png')?>" class="img-fluid">
                <a>
                    <div class="mask rgba-white-slight"></div>
                </a>
            </div>
        </div>
    </div>
</div>