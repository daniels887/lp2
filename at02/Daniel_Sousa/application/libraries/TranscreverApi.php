<?php
require 'vendor/autoload.php';
        
# Imports the Google Cloud client library
use Google\Cloud\Speech\V1\SpeechClient;
use Google\Cloud\Speech\V1\RecognitionAudio;
use Google\Cloud\Speech\V1\RecognitionConfig;
use Google\Cloud\Speech\V1\RecognitionConfig\AudioEncoding;

class TranscreverApi {

    public function speech($audio, $language){

        
        putenv('GOOGLE_APPLICATION_CREDENTIALS=C:\xampp\htdocs\lp2/at02/Daniel_Sousa\ProjetoFaculdade-e927d9058c56.json');
        # The name of the audio file to transcribe
        $audioFile = './assets/audio/'.$audio;
        # get contents of a file into a string
        $content = file_get_contents($audioFile);
        
        # set string as audio content
        $audio = (new RecognitionAudio())
            ->setContent($content);
        
        # The audio file's encoding, sample rate and language
        $config = new RecognitionConfig([
            'encoding' => AudioEncoding::FLAC,
            'language_code' => $language
        ]);
        
        # Instantiates a client
        $client = new SpeechClient();
        
        # Detects speech in the audio file
        $response = $client->recognize($config, $audio);
        
        # Print most likely transcription
        foreach ($response->getResults() as $result) {
            $alternatives = $result->getAlternatives();
            $mostLikely = $alternatives[0];
            $transcript = $mostLikely->getTranscript();
        }
        return $transcript;
    }
}
